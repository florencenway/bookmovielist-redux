import React from 'react';
import './App.css';
import BookList from './pages/BookList';
import MovieList from './pages/MovieList';

function App() {
  return (
    <div className="App">
      <h2>Books</h2>
      <BookList />
      <h2>Movies</h2>
      <MovieList />
    </div>
  );
}

export default App;