import { SELECT_MOVIE } from "./movieActionTypes";

export function movieSelector(movie) {
    return {
      type: SELECT_MOVIE,
      payload: movie,
    };
}