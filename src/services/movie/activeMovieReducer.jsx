import { SELECT_MOVIE } from "./movieActionTypes";

export default function (state = null, action) {
    switch(action.type){
        case SELECT_MOVIE:
            console.log(action)
            return action.payload;
        default:
            return state;    
    }
}