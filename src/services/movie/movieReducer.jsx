export default function () {
  return [
    { title: "Harry Potter Movie", duration: "60 mins" },
    { title: "Batman Movie", duration: "50 mins" },
    { title: "Marvels Movie", duration: "90 mins" },
    { title: "Superman Movie", duration: "120 mins" },
  ];
}
