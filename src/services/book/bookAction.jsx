import {SELECT_BOOK} from './bookActionTypes';

export function selectBook(book) {
    console.log(book)
    return {
        type: SELECT_BOOK,
        payload: book
    };
}