import {SELECT_BOOK} from "./bookActionTypes";

export default function (state = null, action) {
    switch(action.type) {
        case SELECT_BOOK:
            console.log(action);
            return action.payload;
        default:
            return state;    
    }
}