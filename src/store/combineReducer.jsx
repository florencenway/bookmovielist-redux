import {combineReducers} from "redux";
import bookReducer from "../services/book/bookReducer";
import movieReducer from "../services/movie/movieReducer";
import activeBookReducer from "../services/book/activeBookReducer";
import activeMovieReducer from "../services/movie/activeMovieReducer";


const rootReducer = combineReducers({
  bookReducer: bookReducer,
  movies: movieReducer,
  activeBookReducer: activeBookReducer,
  activeMovie: activeMovieReducer
});

export default rootReducer;