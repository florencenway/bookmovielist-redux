import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { movieSelector } from "../services/movie/movieAction";
import MovieDetail from "./containers/MovieDetail";

const MovieList = () => {
  //const movies = useSelector(state => state.movies);
  const movies = useSelector(({movies}) => movies);
  const dispatch = useDispatch()
  
  const movieclickHandler = (movie) => {
    console.log(movie)
    dispatch(movieSelector(movie));
  }
  

  const renderList = () => {
    return movies.map((movie) => {
      return (
        <li
          key={movie.title}
          className="list-group-item"
          onClick={() => movieclickHandler(movie)}
        >
          {movie.title}
        </li>
      );
    });
  };
  return <div>
            <ul className="list-group col-sm-4">{renderList()}</ul>
            <MovieDetail/>
        </div>
};



export default MovieList
