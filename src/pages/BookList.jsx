import React from 'react';
import {connect} from "react-redux";
import { bindActionCreators } from "redux";
import {selectBook} from "../services/book/bookAction";
import BookDetail from './containers/BookDetail';


 const BookList = (props) => {
     const {books,selectBook} = props;

     const renderList = () => {
         return books.map(book => {
             return <li key={book.title} className="list-group-item" onClick={() => selectBook(book)}>{book.title}</li>
         })
     }
    return (
      <div>
        <ul className="list-group col-sm-4">{renderList()}</ul>
        <BookDetail/>
      </div>
    );
}

const mapStateToProps = state => {
    //console.log('state ==> ',state)
    return {
        books: state.bookReducer
    }
}
// const mapStateToProps = ({bookReducer, movies}) => {
//   return {
//     books: bookReducer,
//     movies: movies, 
//   };
// };

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({selectBook}, dispatch) //selectedBook = selectedBook
}

export default connect(mapStateToProps, mapDispatchToProps)(BookList); //mapStateToProps(state,action)