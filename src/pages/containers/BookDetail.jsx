import React from 'react';
import {connect} from 'react-redux';

const BookDetail = (props) => {
    const {activeBook} = props;

    return (
      <div className="m-5">
        <h1>Detail</h1>
        <div>Title: {activeBook?.title}</div>
        <div>Pages: {activeBook?.pages}</div>
      </div>
    );
}

const mapStateToProps = ({activeBookReducer}) => ({
    activeBook: activeBookReducer //from rootReducer in combineReducer
})


export default connect(mapStateToProps)(BookDetail);