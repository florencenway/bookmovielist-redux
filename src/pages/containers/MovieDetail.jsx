import React from 'react';
import {useSelector} from 'react-redux';

const MovieDetail = () => {
    
    const activeMovie = useSelector(state => state.activeMovie);
    console.log(activeMovie)

    return (
      <div className="m-5">
        <h1>Detail</h1>
        <div>Title: {activeMovie?.title}</div>
        <div>Duration: {activeMovie?.duration}</div>
      </div>
    );
}



export default MovieDetail;